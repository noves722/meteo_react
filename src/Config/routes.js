import React from "react";

//PAGES
import Login from '../pages/Login'
import Logout from '../pages/Logout'
import NotFound from '../pages/NotFound'
import Home from "../pages/Home";
import Location from '../pages/Location';

import LocationParam from "../components/LocationParam";

// Config/routes.js

const routes = [
    {
      path: "/",
      element: <Login />,
    },
    {
      path: "/logout",
      element: <Logout />,
    },
    {
      path: "/*",
      element: <NotFound />,
    },
    {
        path: "/home",
        element: <Home />,
    },
    {
        path: "/location",
        element: <Location />,
    },
    {
     path: "/location/:idProv/:idTown",
     element: <LocationParam/>,
    },
    
  ];
  
  export default routes;