// Context/context.js

import React, { useReducer,useContext } from "react";
import { initialState, AuthReducer } from "./reducer";

/*******************************************************/
//CONTEXTOS
/*******************************************************/
const AuthStateContext = React.createContext();
const AuthDispatchContext = React.createContext();

/*******************************************************/
//HOCK'S CUSTOMIZADOS
/*******************************************************/
//PARA USARA EL ESTADO DEL AUTH
export function useAuthState() {
  const context = useContext(AuthStateContext);
  if (context === undefined) {
    throw new Error("useAuthState debe utilizarse dentro de un AuthProvider");
  }
  return context;
}
//PARA USARA EL DISPARADOR DE EVENTOS DEL AUTH
export function useAuthDispatch() {
  const context = useContext(AuthDispatchContext);
  if (context === undefined) {
    throw new Error(
      "useAuthDispatch debe utilizarse dentro de un AuthProvider"
    );
  }
  return context;
}

/*******************************************************/
//PROVIDERS
//COMBINA LOS PROVIDER'S DE LOS DOS CONTEXTOS
//RECIBE COMPONENSTES HIJOS Y MANEJA EL ESTADO CON useRedux
//NECESITA FUNCION REDUCTORA Y EL ESTADO INICIAL
/*******************************************************/
export const AuthProvider = ({ children }) => {
    const [user, dispatch] = useReducer(AuthReducer, initialState);
  //DISPARAMOS PARA ABAJO EL USUARIO Y EL DISPARADOR 
  //DE FORMA INDEPENDIENTE PORQUE TODOS NECESITAN EL USER
  // PERO NO TODOS EL DISPARADOR SOLO LOGIN Y LOGOUT
    return (
      <AuthStateContext.Provider value={user}>
        <AuthDispatchContext.Provider value={dispatch}>
          {children}
        </AuthDispatchContext.Provider>
      </AuthStateContext.Provider>
    );
  };

  