

import signIn from "../utils/userValid";

export async function loginUser(dispatch, formData) {

  
  try {
    dispatch({ type: 'REQUEST_LOGIN' });
      const response = await signIn(formData.email, formData.password);
      //const  data  = await response.json();
      console.log ("response actions  ", response)
      const data = response
      console.log('data= ', data)
   
    
    if (data) {

      // Para lanzar el evento de login con éxito y la data del usuario
      dispatch({ type: 'LOGIN_SUCCESS', payload: data });
      // Para mantener la sesión activa en Chrome
      localStorage.setItem('currentUser', JSON.stringify(data));
      console.log('local storage . . . ',localStorage.getItem("currentUser"))
      
      return data;
    } 
   
   

    dispatch({ type: 'LOGIN_ERROR', error: 'error' });
    return;
  } catch (error) {

    dispatch({ type: 'LOGIN_ERROR', error: error });
    
  }
}

export async function logout(dispatch) {
  dispatch({ type: 'LOGOUT' });
  localStorage.removeItem('currentUser');
  console.log('dentro de logout ',localStorage.getItem("currentUser"))
  
}
