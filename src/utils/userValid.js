

const users = [
    { email: "chema@test.com", password: "111", name:"Chema" },
    { email: "lara@test.com", password: "123",  name:"Lara"},
    { email: "lola@test.com", password: "000", name:"Lola"}
  ];
  
  function signIn(email, password) {
    const user = users.find(
      (user) => user.email === email && user.password === password
    );
    if (user === undefined) throw new Error();
    return user;
  }

  export default signIn
  