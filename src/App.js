import './App.css';
import React from 'react'
import { Link, Route, Routes,  } from "react-router-dom";
//import Home from "./pages/Home";
//import Community from './pages/Community';
//mport Location from './pages/Location';

//importamos configuracion de las rutas
import routes from "./Config/routes.js";

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
//import Maps from './components/Maps';
function App() {
  return (
    <div className="App">
      <header className='headerApp'>
      <Link to="/">
          <button className="btn btn-primary button">Login</button>
        </Link>
        <Link to="/home">
          <button className="btn btn-primary button">Home</button>
        </Link>
        <Link to="/location">
          <button className="btn btn-primary button">Location</button>
        </Link>
        <Link to="/logout">
          <button className="btn btn-primary button">Logout</button>
        </Link>
      </header>
      <Routes>
      {routes.map((route) => (
          <Route
            key={route.path}
            path={route.path}
            element={route.element}
          />
      ))}
      </Routes> 
      <footer>
        <div className='footerApp'>
          <h3>Aplicacion creada con "React"</h3>
          <h4>Desarrollada por Chema-nv <img class="imagen" src="/images/chema.png"></img></h4>
          <h5>Los datos han sido obtenidos de:</h5>
          <h6>https://www.el-tiempo.net/api</h6>

        </div>
      </footer>
      
    </div>
  );
}

export default App;
