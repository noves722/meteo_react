import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const MeteoTown = (props) => {
  //debugger
  //console.log (props.stateLocation);
  const { idProv, idTown } = useParams();
  //Definimos el array de posts
  let [meteoCity, setMeteoCity] = useState({});


  //Queremos mostrar un loading mientras carga
  let [isLoadingMet, setIsLoadingMet] = useState(false);
  const city = props.stateLocation;

  if (city.selectTown) {
    city.selectTown = false;
    setIsLoadingMet = {};
  }

  //console.log(url);
  console.log(city.name);
  console.log(city.idProv);
  console.log(city.idTown);

  //const prov =city.idProv;
  //const munic =city.idTown;
  const url = `https://www.el-tiempo.net/api/json/v2/provincias/${city.idProv}/municipios/${city.idTown}`;
  //const url = `https://www.el-tiempo.net/api/json/v2/provincias/${idProv}/municipios/${idTown}`;
  console.log(url);
  // console.log(url2);
  useEffect(() => {
    setIsLoadingMet(true); //mostramos loading

    fetch(url)
      .then((response) => response.json())

      .then((data) => setMeteoCity(data))

      .finally(() => setIsLoadingMet(false)); //ocultamos el loading
  }, [city]);

  console.log(meteoCity);

  const loading = isLoadingMet ? "Loading..." : null;

  const {
    lluvia,
    pronostico,
    stateSky,
    temperatura_actual,
    temperaturas,
    viento,
  } = meteoCity;

  const getUrlImage = ()=>{
    if (!stateSky) return;
    switch(stateSky.description){
        case "Despejado": return "http://localhost:3000/images/sunny.png";
        case "Nuboso": return "http://localhost:3000/images/cloud.png";
        case "Brumoso": return "http://localhost:3000/images/bruma.png";
        case "Intervalos nubosos": return "http://localhost:3000/images/cloud.png";
        case "Nubes altas": return "http://localhost:3000/images/cloud.png";
        case "Poco nuboso": return "http://localhost:3000/images/cloud.png";
        case "Cubierto": return "http://localhost:3000/images/cloud.png";
        case "LLuvia": return "http://localhost:3000/images/rain.png"
        default: return "#";
    }
  }
  return (
    <div>
      {loading}
      
      <h3>{city.name}</h3>
      <div className="sky">
        <h4>Cielo</h4>
        {stateSky && <img 
                        src={getUrlImage()} 
                        alt={stateSky.description} 
                        id="weatherIcon"></img>}
      </div>
      <div className="pronostico">
      {pronostico && <img 
                        src='http://localhost:3000/images/amanece.png' 
                        alt='AMANECE' 
                        id="weatherIcon"></img>}
        <h3>{pronostico && pronostico.hoy["@attributes"].orto}</h3>
        {pronostico && <img 
                        src='http://localhost:3000/images/anochece.png' 
                        alt='ANOCHECE' 
                        id="weatherIcon"></img>}
        <h3>{pronostico && pronostico.hoy["@attributes"].ocaso} </h3>
      </div>
      
      <div className="temperatura">
        <h4>TEMPERATURAS</h4>
        <h5>actual {temperatura_actual}º</h5>
        <h6>Max {temperaturas && temperaturas.max}º</h6>
        <h6>Min {temperaturas && temperaturas.min}º</h6>
      </div>
      
    </div>
  );
};

export default MeteoTown;
