import React, {useState, useEffect} from 'react'
import './Meteo.css';

const MeteoCommunity = () => {
   //Definimos el array de posts
  let [meteoCom, setMeteoCom] = useState([]);
  // loading mientras carga
  let [isLoading, setIsLoading] = useState(false); 
  useEffect(() => {
    setIsLoading(true);//mostramos loading
    fetch("https://www.el-tiempo.net/api/json/v2/provincias")
      .then(response => response.json())
      
      .then(data => setMeteoCom(data.provincias))
      
      .finally(() => setIsLoading(false));//ocultamos el loading
  }, []);


 
  
  const loading = isLoading ? 'Loading...' : null;
  return (
    <fieldset>
      <div className="container"> 
        { loading  }  
        
          { meteoCom.map((metCom, key) => (
          <div key={ key } className= "cardd">

            {/* <h7>{ metCom.COMUNIDAD_CIUDAD_AUTONOMA }</h7> */}
            
            <p>{ metCom.NOMBRE_PROVINCIA }</p>
            
          </div>))
        } 
      </div>
    </fieldset>
  )
}

export default MeteoCommunity