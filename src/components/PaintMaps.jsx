import React from 'react'
import GoogleMaps from "simple-react-google-maps";

const PaintMaps = (props) => {
  return (
    <div>
      <GoogleMaps
            apiKey={"AIzaSyCPdoP74-OP4-mgbkSsENCnSIhGU2-5I70"}
            style={{ height: "400px", width: "300px" }}
            zoom={14}
            center={{
              lat: props.latitude,
              lng: props.longitude,
            }}
            markers={[{ lat: props.latitude, lng: props.longitude }]}
          />
    </div>
  )
}

export default PaintMaps
