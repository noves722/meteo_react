import React from "react";
import Geocode from "react-geocode";

let address = undefined;

const Geolocation = (props) => {
  
  
  // configurar la API de codificación geográfica de Google Maps para fines de administración de cuotas. Es opcional pero recomendado.
  Geocode.setApiKey("AIzaSyBnT7epRQcwxlQgB7Y7KQyqiajFnmO5FsM");

  // establecer el idioma de respuesta. Predeterminado en inglés.
  Geocode.setLanguage("es");

  // establecer la región de respuesta. es opcional.
  // Una solicitud de geocodificación con region=es (España) devolverá la ciudad española
  Geocode.setRegion("es");

  // opcional
  // google geocoder devuelve más de una dirección para la lat/lng dada.
  // En algunos casos, necesitamos una dirección como respuesta para la cual Google proporciona un filtro de tipo de ubicación.
  // Entonces podemos analizar fácilmente el resultado para obtener los componentes de la dirección
  // ROOFTOP, RANGE_INTERPOLATED, GEOMETRIC_CENTER, APPROXIMATE son los valores aceptados.
  // Y de acuerdo con los documentos de Google a continuación en la descripción, el parámetro ROOFTOP devuelve el resultado más precisot.
  Geocode.setLocationType("ROOFTOP");

  // Habilitar o deshabilitar registros. es opcional.
  Geocode.enableDebug();

  // Obtener dirección de latitud y longitud.
  Geocode.fromLatLng(props.latitud, props.longitud).then(
    (response) => {
      address = response.results[0].formatted_address;
      console.log(address);
    },
    (error) => {
      console.error(error);
    }
  );
  return (
  <div>
    <p>{address}</p>
    <p>LAT: {props.latitud}  ,  LONG: {props.longitud}</p>
  </div>  
  
  );
};

export default Geolocation;
