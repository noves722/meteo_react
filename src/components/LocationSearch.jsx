import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useAuthState } from "../Context";

import "./Meteo.css";

//para la internacionalizacion
import { FormattedMessage } from "react-intl";

const LocationSearch = () => {
  //PARA TRAERNOS EL USUARIO LOGADO
  const { userDetails } = useAuthState();

  //ESTADO PARA EL TEXTO DEL BUSCADOR
  const [text, setText] = useState("");
  //ESTADO PARA CONTROLAR LA BUSQUEDA
  const [search, setSearch] = useState([]);
  //ESTADO QUE CONTROLA EL FINAL DE LA BUSQUEDA

  //Definimos el array de posts
  let [meteoLoc, setMeteoLoc] = useState([]);
  //Queremos mostrar un loading mientras carga
  let [isLoading, setIsLoading] = useState(false);
  const municipios = "municipios";
  useEffect(() => {
    setIsLoading(true); //mostramos loading
    fetch(`https://www.el-tiempo.net/api/json/v2/${municipios}`)
      .then((response) => response.json())

      .then((data) => setMeteoLoc(data))

      .finally(() => setIsLoading(false)); //ocultamos el loading
  }, []);
  //console.log(meteoLoc)
  //ORDENO LA VERIABLE CON TODAS LAS POBLACIONES POR LAS MAS POBLADAS
  //ASI EN LA MUESTRA SACA PRIMERO LAS MAS POBLADAS
  meteoLoc.sort((a, b) => b.POBLACION_MUNI - a.POBLACION_MUNI);

  //FUNCION QUE OBTIENE SI SE HA PULSADO UNA TECLA DEL TECLADO
  //SETEANDO EL ESTADO DE TEXT PARA IR GURADANDO LAS LETRAS
  const handleText = (e) => {
    setText(e.target.value);
  };

  useEffect(() => {
    //SI NO SE HA ESCRITO NADA SETEA ARRAY VACIO Y RETORNA
    if (!text) {
      setSearch([]);
      return;
    }
    //EN RESULT NOS QUEDAMOS CON LAS LOCALIDADES QUE INCLUYEN LA CADEMA QUE INTRODICIMOS
    //GUARDADA EN text Y ADEMAS QUE ESA CADENA ESTE EN LA POSICION 0 AL PRINCIPIO DE LA PALABRA
    //SE USA toLowerCase EN text Y En element.NOMBRE PARA QUE FUNCIONE LA BUSQUEDA CON MINUSCULAS Y MALLUSCULAS
    let result = meteoLoc.filter(
      (element) =>
        element.NOMBRE.toLowerCase().indexOf(text.toLowerCase()) === 0
    );
    //SETEAMOS EL RESULTADO DE SEARCH CON LA MODIFICACION QUE HEMOS HECHO
    setSearch(result);
    //debugger
  }, [text]);

  const loading = isLoading ? "Loading..." : null;

  return (
    <div className="container">
      {!userDetails && (
        <div>
          <h3>Upsss</h3>
          <FormattedMessage
            id="location.pSearch"
            defaultMessage="!Sorry..¡ bonito, bonita o bonite, deberias logarte antes."
          />
          {/* <p >!Sorry..¡ bonito, bonita o bonite, deberias logarte antes.</p> */}
        </div>
      )}
      {userDetails ? (
        <p>Hola {userDetails.name}, introduce tu busqueda</p>
      ) : (
        <p>No se quien eres</p>
      )}
      {userDetails && (
        <div className="MeteoLoc_div">
          {loading}
          <div >
            {/* PINTAMOS EL IMPUT PARA BUSCAR LAS CIUDADES */}
            <input
              type="search"
              name="townSearch"
              id="search"
              placeholder="Municipio..."
              value={text}
              //AL PULSAR handleText RECOGE Y GUARDA LA LETRA
              onChange={handleText}
              className="imputSearch"
            />
          </div>

          <div className="Search_div">
            {search &&
              search.map(
                (el, key) =>
                  key < 10 && (
                    <Link
                      className="cardd"
                      to={`/location/${el.CODPROV}/${el.CODIGOINE.slice(0, 5)}`}
                    >
                      
                        <h5>{el.NOMBRE}</h5>
                        <p>
                          Provincia de {el.NOMBRE_PROVINCIA}, cuenta con{" "}
                          {el.POBLACION_MUNI} habitantes{" "}
                        </p>
                      
                    </Link>
                  )
              )}
          </div>

          <div></div>
        </div>
      )}
    </div>
  );
};

export default LocationSearch;
