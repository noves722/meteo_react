import React, { Component } from "react";
import GoogleMaps from "simple-react-google-maps";
//instlamos la dependencia  google-maps
//npm install --force --save ssimple-react-google-maps
//el siguiente video esplica como crear clave api cuenta google cloud, 
// instalar api javascript google maps y luego crear api key
//https://www.youtube.com/watch?v=AZUv7DVyHO4

export default class Maps extends Component {
  render() {
    return (
        
      <div className="container">
        <GoogleMaps

          apiKey={"AIzaSyCPdoP74-OP4-mgbkSsENCnSIhGU2-5I70"}
          style={{ height: "400px", width: "300px" }}
          zoom={1}
          center={{
            lat: 40.4127355,
            lng: -3.695428
          }}
          markers={[
            { lat: 40.409711, lng: -3.692569 },
            { lat: 40.412072, lng: -3.676463 },
            { lat: 40.451824, lng: -3.690759 }
          ]}
        />
      </div>
    );
  }
}