import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useAuthState } from "../Context";

const LocationParam = () => {
  //PARA TRAERNOS EL USUARIO LOGADO
  const { userDetails } = useAuthState();

  const { idProv, idTown } = useParams();
  let [meteoCity, setMeteoCity] = useState({});

  //Queremos mostrar un loading mientras carga
  let [isLoadingMet, setIsLoadingMet] = useState(false);

  const url = `https://www.el-tiempo.net/api/json/v2/provincias/${idProv}/municipios/${idTown}`;
  console.log(url);
  // console.log(url2);
  useEffect(() => {
    setIsLoadingMet(true); //mostramos loading

    fetch(url)
      .then((response) => response.json())

      .then((data) => setMeteoCity(data))

      .finally(() => setIsLoadingMet(false)); //ocultamos el loading
  }, []);

  console.log(meteoCity);

  const loading = isLoadingMet ? "Loading..." : null;

  const {
    keywords,
    lluvia,
    pronostico,
    stateSky,
    temperatura_actual,
    temperaturas,
    viento,
  } = meteoCity;

  //const nombre = keywords.includes(",")? keywords.split(",")[1]:'keywords';
  const getUrlImage = () => {
    if (!stateSky) return;
    switch (stateSky.description) {
      
        case "Despejado":
          return "/images/sunny.png";
        case "Nuboso":
          return "/images/cloud.png";
        case "Brumoso":
          return "/images/bruma.png";
        case "Muy nuboso":
          return "/images/cloud.png";
        case "Intervalos nubosos":
          return "/images/cloud.png";
        case "Nubes altas":
          return "/images/cloud.png";
        case "Poco nuboso":
          return "/images/cloud.png";
        case "Cubierto":
          return "/images/cloud.png";
        case "LLuvia":
          return "/images/rain.png";  
      default:
        return "#";
    }
  };
  return (
    <div className="container">
      {loading}
      {userDetails ? <p>Hola {userDetails.name}, aqui tienes tu resultado.</p> : <p>No se quien eres</p>}
      <div className="cardTown">
        <h2>{keywords && keywords.split(",")[0]}</h2>
        <div className="detail">
          {lluvia && (
            <img
              src="http://localhost:3000/images/rainn.png"
              alt="lluvia"
              id="weatherIcon"
            ></img>
          )}
          <h5>{lluvia && lluvia} %</h5>
        </div>
        <div className="detail">
          <h4>Cielo</h4>
          {stateSky && (
            <img
              src={getUrlImage()}
              alt={stateSky.description}
              id="weatherIcon"
            ></img>
          )}
        </div>
        <div className="detail">
          {pronostico && (
            <img
              src="http://localhost:3000/images/amanece.png"
              alt="AMANECE"
              id="weatherIcon"
            ></img>
          )}
          <h5>{pronostico && pronostico.hoy["@attributes"].orto}</h5>
          {pronostico && (
            <img
              src="http://localhost:3000/images/anochece.png"
              alt="ANOCHECE"
              id="weatherIcon"
            ></img>
          )}
          <h5>{pronostico && pronostico.hoy["@attributes"].ocaso} </h5>
        </div>
        <div className="detail">
          {viento && (
            <img
              src="http://localhost:3000/images/wind.png"
              alt="viento"
              id="weatherIcon"
            ></img>
          )}
          <h5>{viento && viento} Km/h</h5>
        </div>
        <div className="temperatura">
          <h4>TEMPERATURAS</h4>
          <h5>actual {temperatura_actual}º</h5>
          <h6>Max {temperaturas && temperaturas.max}º</h6>
          <h6>Min {temperaturas && temperaturas.min}º</h6>
        </div>
      </div>
    </div>
  );
};

export default LocationParam;
