import React, { useState } from "react";
import styles from "./login.module.css";
import { useNavigate } from "react-router-dom";

// Auth context
import { loginUser, useAuthState, useAuthDispatch } from "../../Context";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  //obtengo el despachador
  const dispatch = useAuthDispatch();

  const { loading, errorMessage } = useAuthState();

  let navigate = useNavigate();

  const handleLogin = async (e) => {
    e.preventDefault();
    let formData = { email, password };
    //console.log(formData);
    try {
      //debugger
      let response = await loginUser(dispatch, formData); //loginUser action realiza la petición y gestiona todos los cambios de estado necesarios
      //debugger
      if (!response) {
        navigate("/");
        //return
      }
      navigate("/home");
      window.location.reload();
      //si todo sale bien saltaremos a home
    } catch (error) {
      //console.log(error)
    }
    // petición asíncrona al servidor
  };

  return (
    <div className={styles.container}>
      <div className={styles.formContainer}>
        <h1>Login Page</h1>
        {errorMessage ? <p className={styles.error}>{errorMessage}</p> : null}
        <form>
          <div className={styles.loginForm}>
            <div className={styles.loginFormItem}>
              <label htmlFor="email">Username</label>
              <input
                type="text"
                id="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div className={styles.loginFormItem}>
              <label htmlFor="password">Password</label>
              <input
                type="password"
                id="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
          </div>
          <button onClick={handleLogin} className="btn btn-primary button">
            login
          </button>
        </form>
        <p>PRUEBAS "lola@test.com",password: "000" </p>
      </div>
    </div>
  );
}
