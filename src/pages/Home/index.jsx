import React, { useEffect, useState } from "react";
//import React from 'react'
import styles from "./home.module.css";

import Geolocation from "../../components/Geolocation";

import PaintMaps from "../../components/PaintMaps";
import { useAuthState } from "../../Context";

const Home = () => {
  //PARA TRAERNOS EL USUARIO LOGADO
  const { userDetails } = useAuthState();
  console.log('home user...', userDetails);

  const [state, setState] = useState({
    longitude: 0,
    latitude: 0,
  });

  //https://www.youtube.com/watch?v=NfDTO4c0xLc
  useEffect(() => {
    navigator.geolocation.getCurrentPosition(
      function(position) {
        setState({
          longitude: position.coords.longitude,
          latitude: position.coords.latitude,
        });
      },

      function(error) {
        console.error("Error Code = " + error.code + " - " + error.message);
      },
      {
        enableHighAccuracy: true,
      }
    );
  }, []);

  return (
    <div className={styles.container}>
      {userDetails ? <p>Te encuentras aqui, {userDetails.name}.</p> : <p>No se quien eres</p>}
      <h2>Geolocation</h2>
      {!userDetails && (
        <div>
          <h3>Upsss</h3>
          <p>!Sorry..¡ bonito, bonita o bonite, deberias logarte antes.</p>
        </div>
      )}
      {userDetails && (
        <div>
          <Geolocation latitud={state.latitude} longitud={state.longitude} />
          {state.latitude > 0 && (
            <PaintMaps latitude={state.latitude} longitude={state.longitude} />
          )}
        </div>
      )}
    </div>
  );
};

export default Home;
