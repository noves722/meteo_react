// Pages/Dashboard/index.js
 
import React from 'react'
import styles from './logout.module.css'
import { logout, useAuthDispatch, useAuthState } from '../../Context';
import { useNavigate } from 'react-router-dom';

function Logout() {
    let navigate = useNavigate();
  const dispatch = useAuthDispatch();
  const { userDetails } = useAuthState();


    const handleLogout = () => {
        logout(dispatch);       
        navigate("/");
        window.location.reload();
      };
    
    return (
        <div className={styles.logoutPage}>
            {userDetails ? <p>Quieres irte, {userDetails.name}.</p> : <p>No se quien eres</p>}
            <div  >
                <h3>
                    Si te vas dime adios y pulsa logout...
                </h3>
                <button  className="btn btn-primary button " onClick={handleLogout}>Logout</button>
            </div>
            <p>Welcome to the logout</p>
            
        </div>
    )
}
 
export default Logout