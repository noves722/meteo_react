import React from "react";
import LocationSearch from "../../components/LocationSearch";
import styles from "./location.module.css";

const Location = () => {
  return (
    <div className={styles.locationPage}>
      <LocationSearch />
    </div>
  );
};

export default Location;
