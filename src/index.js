import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import { AuthProvider } from "./Context";

import { BrowserRouter as Router } from "react-router-dom";

//INTERNACIONALIZACION IMPORT
import { IntlProvider } from "react-intl";
import Spanish from "./lang/es.json";
import English from "./lang/en.json";
import French from "./lang/fr.json";
/////////INTERNACIONALIZACION IMPORT///////////



//INTERNACIONALIZACION  VARIABLES
const locale = navigator.language;
//const locale = 'fr'
console.log(locale)
var lang;
if (locale === "en") {
  lang = English;
} else {
  if (locale === "es-ES") {
    lang = Spanish;
  } else {
    lang = French;
  }
}


const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <React.StrictMode>    
    <AuthProvider>  {/* EL CONTEXTO ENVUELVE TODA LA APLICACION */}
      {/* LOS CHILDRENS DE AuthProvide ES TODO LO QUE HAY DENTRO DE Router */}
      <IntlProvider locale={locale} messages={lang}>
      <Router>
        <App />
      </Router>
      </IntlProvider>
    </AuthProvider>
  </React.StrictMode>
);
